<?php

namespace FeatureControlSystem;

/**
 * Class FCS
 */
class FCS
{

    /**
     * Constants of flags and their integers
     */
    const
        FLAG_INACTIVE = 0,
        FLAG_ACTIVE = 1,
        FLAG_COOKIE = 2,
        FLAG_TAG = 3,
        FLAG_MULTI_AND = 4,
        FLAG_MULTI_OR = 5,
        FLAG_TAG_RANGE = 6,
        FLAG_TAG_EXCLUDED = 7;

    /**
     * Singleton
     * @var null|FCS
     */
    static private $instance = null;

    /**
     * Variable with configuration data
     * @var array
     */
    private $config = [];

    /**
     * In-memory cache with results of resolving flags for features
     * @var array
     */
    private $cache = [];

    /**
     * Creates singleton instance
     * @return FCS
     */
    public static function Instance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * FCS constructor.
     * @return void
     */
    private function __construct()
    {
        $this->reset();
    }

    /**
     * Clear in-memory cache
     * @return void
     */
    public function clearCache()
    {
        $this->cache = [];
    }

    /**
     * Resets configuration data
     * @return void
     */
    public function reset()
    {
        $this->config = [
            'tags' => [
                'env' => 'staging',
                'username' => '',
                'IP' => !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0',
                'role' => 'user'
            ],
            'flags' => []
        ];
        $this->clearCache();
    }

    /**
     * Sets tag's name and value
     * @param string $name
     * @param string|array $value
     * @return void
     */
    public function setTag($name, $value)
    {
        $this->config['tags'][$name] = $value;
        $this->clearCache();
    }

    /**
     * Set list of features with details of flags
     * @param array $flags
     * @return void
     */
    public function setFlags($flags)
    {
        $this->config['flags'] = $flags;
        $this->clearCache();
    }

    /**
     * Load list from redis cache
     * @param object $redis
     * @param string $key
     * @return void
     */
    public function loadFlags($redis, $key)
    {
        $this->config['flags'] = json_decode($redis->get($key), true);
        $this->clearCache();
    }

    /**
     * Returns list of active features
     * @return array
     */
    public function getActiveFeatures()
    {
        $activeFeatures = [];
        foreach ($this->config['flags'] as $name => $flag) {
            if ($this->feature($name)) {
                $activeFeatures[] = $name;
            }
        }
        return $activeFeatures;
    }

    /**
     * Returns list of CSS classes for frontend for <body> element
     * @return string
     */
    public function getBodyCssClasses()
    {
        $features = [];
        foreach ($this->config['flags'] as $name => $flag) {
            if ($this->feature($name) && isset($flag['css']) && $flag['css'] === true) {
                $features[] = $name;
            }
        }
        $bodyClasses = '';
        foreach ($features as $feature) {
            $bodyClasses .= ($bodyClasses ? ' ' : '') . 'fcs-' . str_replace('/', '--', $feature);
        }
        return $bodyClasses;
    }

    /**
     * Checks if feature is active
     * @param string $name
     * @return bool
     */
    public function feature($name)
    {
        if (isset($this->cache[$name])) {
            return $this->cache[$name];
        } else if (!empty($this->config['flags'][$name])) {
            $flag = $this->config['flags'][$name];
            return $this->cache[$name] = $this->isFlagActive($name, $flag);
        } else {
            return false;
        }
    }

    /**
     * Validates flag details to check if feature is active
     * @param string $name
     * @param array $flag
     * @return bool
     */
    private function isFlagActive($name, $flag)
    {
        if (!empty($flag['flag'])) {
            switch ($flag['flag']) {

                case self::FLAG_INACTIVE:
                    return false;
                    break;

                case self::FLAG_ACTIVE:
                    return true;
                    break;

                case self::FLAG_COOKIE:
                    return isset($_COOKIE['fcs-' . str_replace('/', '--', $name)]);
                    break;

                case self::FLAG_TAG:
                    return isset($flag['name'])
                        && isset($flag['value'])
                        && isset($this->config['tags'][$flag['name']])
                        && (is_array($flag['value'])
                            ? in_array($this->config['tags'][$flag['name']], $flag['value'])
                            : $this->config['tags'][$flag['name']] === $flag['value']);
                    break;

                case self::FLAG_TAG_RANGE:
                     if (isset($flag['name'])
                        && (
                            (
                                isset($flag['min_value'])
                                && is_numeric($flag['min_value'])
                            )
                            || (
                                isset($flag['max_value'])
                                && is_numeric($flag['max_value'])
                            )
                        )
                        && isset($this->config['tags'][$flag['name']])
                        && is_numeric($this->config['tags'][$flag['name']])) {
                         $value = $this->config['tags'][$flag['name']];
                         return (!isset($flag['min_value']) || $value >= $flag['min_value']) && (!isset($flag['max_value']) || $value <= $flag['max_value']);
                     }
                    break;

                case self::FLAG_TAG_EXCLUDED:
                    return isset($flag['name'])
                        && isset($flag['value'])
                        && isset($this->config['tags'][$flag['name']])
                        && (is_array($flag['value'])
                            ? !in_array($this->config['tags'][$flag['name']], $flag['value'])
                            : $this->config['tags'][$flag['name']] !== $flag['value']);
                    break;

                case self::FLAG_MULTI_AND:
                    if (!empty($flag['flags'])) {
                        foreach ($flag['flags'] as $subflag) {
                            if (!$this->isFlagActive($name, $subflag)) {
                                return false;
                            }
                        }
                        return true;
                    }
                    break;

                case self::FLAG_MULTI_OR:
                    if (!empty($flag['flags'])) {
                        foreach ($flag['flags'] as $subflag) {
                            if ($this->isFlagActive($name, $subflag)) {
                                return true;
                            }
                        }
                        return false;
                    }
                    break;
            }
        }
        return false;
    }

}