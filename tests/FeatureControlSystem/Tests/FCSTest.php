<?php

namespace FeatureControlSystem\Tests;

use PHPUnit\Framework\TestCase;
use \FeatureControlSystem\FCS;

/**
 * Class FCSTest
 */
class FCSTest extends TestCase
{

    protected function setUp()
    {
        FCS::Instance()->reset();
        $_COOKIE = [];
    }

    public function testFeatureWithInactiveFlag()
    {
        FCS::Instance()->setFlags([
            'my-feature' => ['flag' => FCS::FLAG_INACTIVE]
        ]);
        $this->assertFalse(FCS::Instance()->feature('my-feature'));
    }

    public function testFeatureWithActiveFlag()
    {
        FCS::Instance()->setFlags([
            'my-feature' => ['flag' => FCS::FLAG_ACTIVE]
        ]);
        $this->assertTrue(FCS::Instance()->feature('my-feature'));
    }

    public function testFeatureWithCookieFlagAndMissingCookie()
    {
        FCS::Instance()->setFlags([
            'my-feature' => ['flag' => FCS::FLAG_COOKIE]
        ]);
        $this->assertFalse(FCS::Instance()->feature('my-feature'));
    }

    public function testFeatureWithCookieFlagAndExistingCookie()
    {
        FCS::Instance()->setFlags([
            'my-feature' => ['flag' => FCS::FLAG_COOKIE]
        ]);
        $_COOKIE['fcs-my-feature'] = 1;
        $this->assertTrue(FCS::Instance()->feature('my-feature'));
    }

    public function testFeatureWithCookieFlagAndExistingCookieAndSlashName()
    {
        FCS::Instance()->setFlags([
            'test/my-feature' => ['flag' => FCS::FLAG_COOKIE],
            'test/my-feature2' => ['flag' => FCS::FLAG_COOKIE]
        ]);
        $_COOKIE['fcs-test--my-feature'] = 1;
        $_COOKIE['fcs-test/my-feature2'] = 1;
        $this->assertTrue(FCS::Instance()->feature('test/my-feature'));
        $this->assertFalse(FCS::Instance()->feature('test/my-feature2'));
    }

    public function testFeatureWithTagFlag()
    {
        FCS::Instance()->setFlags([
            'my-feature' => ['flag' => FCS::FLAG_TAG, 'name' => 'country', 'value' => ['UK', 'DE']],
            'my-feature2' => ['flag' => FCS::FLAG_TAG, 'name' => 'product', 'value' => ['product2', 'product3']],
            'my-feature3' => ['flag' => FCS::FLAG_TAG, 'name' => 'lang', 'value' => 'en'],
        ]);
        FCS::Instance()->setTag('country', 'UK');
        FCS::Instance()->setTag('product', 'product1');
        FCS::Instance()->setTag('lang', 'en');
        $this->assertTrue(FCS::Instance()->feature('my-feature'));
        $this->assertFalse(FCS::Instance()->feature('my-feature2'));
        $this->assertTrue(FCS::Instance()->feature('my-feature3'));
    }

    public function testFeatureWithTagExcludedFlag()
    {
        FCS::Instance()->setFlags([
            'my-feature' => ['flag' => FCS::FLAG_TAG_EXCLUDED, 'name' => 'country', 'value' => ['UK', 'DE']],
            'my-feature2' => ['flag' => FCS::FLAG_TAG_EXCLUDED, 'name' => 'product', 'value' => ['product2', 'product3']],
            'my-feature3' => ['flag' => FCS::FLAG_TAG_EXCLUDED, 'name' => 'lang', 'value' => 'en'],
        ]);
        FCS::Instance()->setTag('country', 'UK');
        FCS::Instance()->setTag('product', 'product1');
        FCS::Instance()->setTag('lang', 'en');
        $this->assertFalse(FCS::Instance()->feature('my-feature'));
        $this->assertTrue(FCS::Instance()->feature('my-feature2'));
        $this->assertFalse(FCS::Instance()->feature('my-feature3'));
    }

    public function testFeatureWithTagRangeFlag()
    {
        FCS::Instance()->setFlags([
            'my-feature' => ['flag' => FCS::FLAG_TAG_RANGE, 'name' => 'money', 'min_value' => 5],
            'my-feature2' => ['flag' => FCS::FLAG_TAG_RANGE, 'name' => 'money', 'max_value' => 10],
            'my-feature3' => ['flag' => FCS::FLAG_TAG_RANGE, 'name' => 'money', 'min_value' => 6, 'max_value' => 8],
        ]);

        FCS::Instance()->setTag('money', 5);

        $this->assertTrue(FCS::Instance()->feature('my-feature'));
        $this->assertTrue(FCS::Instance()->feature('my-feature2'));
        $this->assertFalse(FCS::Instance()->feature('my-feature3'));

        FCS::Instance()->setTag('money', 3);

        $this->assertFalse(FCS::Instance()->feature('my-feature'));
        $this->assertTrue(FCS::Instance()->feature('my-feature2'));
        $this->assertFalse(FCS::Instance()->feature('my-feature3'));

        FCS::Instance()->setTag('money', 10);

        $this->assertTrue(FCS::Instance()->feature('my-feature'));
        $this->assertTrue(FCS::Instance()->feature('my-feature2'));
        $this->assertFalse(FCS::Instance()->feature('my-feature3'));

        FCS::Instance()->setTag('money', 7);

        $this->assertTrue(FCS::Instance()->feature('my-feature'));
        $this->assertTrue(FCS::Instance()->feature('my-feature2'));
        $this->assertTrue(FCS::Instance()->feature('my-feature3'));

        FCS::Instance()->setTag('money', 10.1);

        $this->assertTrue(FCS::Instance()->feature('my-feature'));
        $this->assertFalse(FCS::Instance()->feature('my-feature2'));
        $this->assertFalse(FCS::Instance()->feature('my-feature3'));

        FCS::Instance()->setTag('money', 15);

        $this->assertTrue(FCS::Instance()->feature('my-feature'));
        $this->assertFalse(FCS::Instance()->feature('my-feature2'));
        $this->assertFalse(FCS::Instance()->feature('my-feature3'));
    }

    public function testFeatureWithMultiAndFlag()
    {
        FCS::Instance()->setFlags([
            'my-feature' => ['flag' => FCS::FLAG_MULTI_AND, 'flags' => [
                ['flag' => FCS::FLAG_TAG, 'name' => 'domain', 'value' => 'domain.com'],
                ['flag' => FCS::FLAG_TAG, 'name' => 'lang', 'value' => 'en']
            ]],
            'my-feature2' => ['flag' => FCS::FLAG_MULTI_AND, 'flags' => [
                ['flag' => FCS::FLAG_TAG, 'name' => 'domain', 'value' => 'domain.com'],
                ['flag' => FCS::FLAG_TAG, 'name' => 'lang', 'value' => 'pl']
            ]],
        ]);
        FCS::Instance()->setTag('domain', 'domain.com');
        FCS::Instance()->setTag('lang', 'en');
        $this->assertTrue(FCS::Instance()->feature('my-feature'));
        $this->assertFalse(FCS::Instance()->feature('my-feature2'));
    }

    public function testFeatureWithMultiOrFlag()
    {
        FCS::Instance()->setFlags([
            'my-feature' => ['flag' => FCS::FLAG_MULTI_OR, 'flags' => [
                ['flag' => FCS::FLAG_TAG, 'name' => 'domain', 'value' => 'domain.com'],
                ['flag' => FCS::FLAG_TAG, 'name' => 'lang', 'value' => 'pl']
            ]],
            'my-feature2' => ['flag' => FCS::FLAG_MULTI_OR, 'flags' => [
                ['flag' => FCS::FLAG_TAG, 'name' => 'domain', 'value' => 'domain2.com'],
                ['flag' => FCS::FLAG_TAG, 'name' => 'lang', 'value' => 'pl']
            ]],
        ]);
        FCS::Instance()->setTag('domain', 'domain.com');
        FCS::Instance()->setTag('lang', 'en');
        $this->assertTrue(FCS::Instance()->feature('my-feature'));
        $this->assertFalse(FCS::Instance()->feature('my-feature2'));
    }

    public function testGetActiveFeatures()
    {
        FCS::Instance()->setTag('env', 'preproduction');
        FCS::Instance()->setTag('username', 'test123');
        FCS::Instance()->setTag('country', 'UK');
        FCS::Instance()->setTag('product', 'product1');
        FCS::Instance()->setTag('domain', 'domain.com');
        FCS::Instance()->setTag('lang', 'en');
        FCS::Instance()->setFlags([
            'experiment' => ['flag' => FCS::FLAG_INACTIVE],
            'blue-background' => ['flag' => FCS::FLAG_TAG, 'name' => 'country', 'value' => ['UK', 'DE']],
            'xmas-promo' => ['flag' => FCS::FLAG_MULTI_AND, 'flags' => [
                ['flag' => FCS::FLAG_TAG, 'name' => 'domain', 'value' => 'domain.com'],
                ['flag' => FCS::FLAG_TAG, 'name' => 'lang', 'value' => 'pl']
            ]],
            'chat' => ['flag' => FCS::FLAG_MULTI_OR, 'flags' => [
                ['flag' => FCS::FLAG_COOKIE],
                ['flag' => FCS::FLAG_TAG, 'name' => 'username', 'value' => ['test123', 'test456']]
            ]],
            'auth' => ['flag' => FCS::FLAG_ACTIVE],
            'new-registration' => ['flag' => FCS::FLAG_TAG, 'name' => 'env', 'value' => 'preproduction'],
            'my-carousel' => ['flag' => FCS::FLAG_COOKIE],
            'test/new-bonus' => ['flag' => FCS::FLAG_TAG, 'name' => 'username', 'value' => ['test123', 'test456']]
        ]);
        $activeFeatures = FCS::Instance()->getActiveFeatures();
        $this->assertEquals($activeFeatures, ['blue-background', 'chat', 'auth', 'new-registration', 'test/new-bonus']);
    }

    public function testGetBodyCssClasses()
    {
        FCS::Instance()->setTag('env', 'preproduction');
        FCS::Instance()->setTag('username', 'test123');
        FCS::Instance()->setTag('country', 'UK');
        FCS::Instance()->setTag('product', 'product1');
        FCS::Instance()->setTag('domain', 'domain.com');
        FCS::Instance()->setTag('lang', 'en');
        FCS::Instance()->setFlags([
            'experiment' => ['flag' => FCS::FLAG_INACTIVE],
            'blue-background' => ['flag' => FCS::FLAG_TAG, 'name' => 'country', 'value' => ['UK', 'DE']],
            'xmas-promo' => ['flag' => FCS::FLAG_MULTI_AND, 'flags' => [
                ['flag' => FCS::FLAG_TAG, 'domain' => 'domain.com'],
                ['flag' => FCS::FLAG_TAG, 'name' => 'lang', 'value' => 'pl']
            ]],
            'chat' => ['flag' => FCS::FLAG_MULTI_OR, 'flags' => [
                ['flag' => FCS::FLAG_COOKIE],
                ['flag' => FCS::FLAG_TAG, 'name' => 'username', 'value' => ['test123', 'test456']]
            ]],
            'auth' => ['flag' => FCS::FLAG_ACTIVE, 'css' => true],
            'new-registration' => ['flag' => FCS::FLAG_TAG, 'name' => 'env', 'value' => 'preproduction', 'css' => false],
            'my-carousel' => ['flag' => FCS::FLAG_COOKIE],
            'test/new-bonus' => ['flag' => FCS::FLAG_TAG, 'name' => 'username', 'value' => ['test123', 'test456'], 'css' => true]
        ]);
        $cssClasses = FCS::Instance()->getBodyCssClasses();
        $this->assertEquals($cssClasses, 'fcs-auth fcs-test--new-bonus');
    }

    public function testLoadingFlagsFromRedis()
    {
        $flags = json_encode([
            'my-feature' => ['flag' => FCS::FLAG_ACTIVE]
        ]);
        $redis = $this->getMockBuilder('\Redis')
            ->setMethods(['get'])
            ->getMock();
        $redis->method('get')
            ->will($this->returnValue($flags));
        $redis->expects($this->once())
            ->method('get')
            ->with($this->equalTo('flags'));
        FCS::Instance()->loadFlags($redis, 'flags');
        $this->assertTrue(FCS::Instance()->feature('my-feature'));
        $this->assertFalse(FCS::Instance()->feature('my-feature2'));
    }

}
