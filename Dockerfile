FROM php:7-cli
MAINTAINER Jerzy Ogonowski

RUN apt-get update && apt-get install zip unzip

#COMPOSER
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

ENV PATH /root/.composer/vendor/bin:$PATH